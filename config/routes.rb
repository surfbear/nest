Rails.application.routes.draw do

  root 'static_pages#home'

  get     '/signup',  to: 'users#new'
  post    '/signup',  to: 'users#create'
  post    '/login',   to: 'sessions#create'
  delete  '/logout',  to: 'sessions#destroy'

  get     '/search',  to: 'static_pages#search'
  get     '/posts',   to: 'static_pages#result'

  get     '/notifications', to: 'notifications#index'
  get     '/stream',  to: 'static_pages#stream'

  resources :users, param: :user_name do
    member do
      get :following, :followers
    end
  end
  resources :likes, only: [:index, :create, :destroy]
  resources :posts, only: [:show, :create, :destroy]
  resources :relationships, only: [:create, :destroy]
end
