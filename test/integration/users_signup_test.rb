require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  # ユーザ登録失敗
  test "invalid signup information" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: {
        user_name: "",
        password:  "",
        password_confirmation: ""
      }}
    end
    assert_template 'users/new' # 登録画面に遷移
    assert flash.empty?
  end

  # ユーザ登録成功
  test "valid signup information" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, params: { user: {
        user_name: "SampleUser",
        password:  "111111",
        password_confirmation: "111111"
      }}
    end
    follow_redirect!
    assert_template 'users/show'
    assert_not flash.empty? # flash[:success]
    assert is_logged_in?
  end

end
