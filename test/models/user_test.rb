require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new(
      user_name: "Example",
      user_screen_name: "ExampleUser",
      password: "foobar",
      password_confirmation: "foobar"
    )
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "user_name should be present" do
    @user.user_name = ""
    assert_not @user.valid?
    @user.user_name = " "
    assert_not @user.valid?
  end

  test "user_name should not be too long" do
    @user.user_name = "a" * 21;
    assert_not @user.valid?
  end

  test "user_name should be unique" do
    duplicate_user = @user.dup
    @user.save
    assert_not duplicate_user.valid?
  end

  # test "user_screen_name should be present" do
  #   @user.user_screen_name = ""
  #   assert_not @user.valid?
  #   @user.user_screen_name = " "
  #   assert_not @user.valid?
  # end

  # test "user_screen_name should not be too long" do
  #   @user.user_screen_name = "a" * 21;
  #   assert_not @user.valid?
  # end

  test "password should be present" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end

  test "should follow and unfollow a user" do
    michael = users(:michael)
    archer  = users(:archer)
    assert_not michael.following?(archer)
    michael.follow(archer)
    assert michael.following?(archer)
    assert archer.followers.include?(michael)
    michael.unfollow(archer)
    assert_not michael.following?(archer)
  end

  test "feed should have the right posts" do
    michael = users(:michael)
    archer  = users(:archer)
    lana    = users(:lana)
    # フォローしているユーザーの投稿を確認
    lana.posts.each do |post_following|
      assert michael.feed.include?(post_following)
    end
    # 自分自身の投稿を確認
    michael.posts.each do |post_self|
      assert michael.feed.include?(post_self)
    end
    # フォローしていないユーザーの投稿を確認
    archer.posts.each do |post_unfollowed|
      assert_not michael.feed.include?(post_unfollowed)
    end
  end

  # ダイジェストが存在しない場合のauthenticated?のテスト
  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?('')
  end

end
