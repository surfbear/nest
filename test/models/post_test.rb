require 'test_helper'

class PostTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    @post = @user.posts.build(content: "foobar")
  end

  test "should be valid?" do
    assert @post.valid?
  end

  test "user_id should be poresent" do
    @post.user_id = nil
    assert_not @post.valid?
  end

  test "content should be present" do
    @post.content = " "
    assert_not @post.valid?
    @post.content = ""
    assert_not @post.valid?
  end

  test "content should not be too long" do
    @post.content = "a" * 1001
    assert_not @post.valid?
  end

end
