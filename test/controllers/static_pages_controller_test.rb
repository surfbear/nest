require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @base_title = "Nest"
  end

  test "should get root" do
    get root_url
    assert_response :success
  end

end
