class User < ApplicationRecord
  has_many :posts
  has_many :likes, dependent: :destroy
  has_many :relationships,
    class_name: "Relationship",
    foreign_key: "follower_id",
    dependent:   :destroy
  has_many :passive_relationships,
    class_name:  "Relationship",
    foreign_key: "followed_id",
    dependent:   :destroy
  has_many :following, through: :relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower

  mount_uploader :icon, IconUploader

  attr_accessor :remember_token

  validates :user_name, presence: true, length: { maximum: 20 }, uniqueness: true, format: { with: /\A\w+\z/ }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
  validate  :icon_size

  # override to_param
  def to_param
    user_name
  end

  def feed
    following_ids = "SELECT followed_id FROM relationships WHERE follower_id = :user_id"
    Post.where("user_id IN (#{following_ids}) OR user_id = :user_id", user_id: id)
  end

  # ユーザをフォローする
  def follow(user)
    relationships.create(followed_id: user.id)
  end

  # ユーザをフォロー解除する
  def unfollow(user)
    relationships.find_by(followed_id: user.id).destroy
  end

  def following?(user)
    following.include?(user)
  end

  # 渡された文字列のハッシュ値を返す
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # ランダムなトークンを返す
  def self.new_token
    SecureRandom.urlsafe_base64
  end

  # 永続セッションのためにユーザーをデータベースに記憶する
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # 渡されたトークンがダイジェストと一致したらtrueを返す
  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  # ユーザーのログイン情報を破棄する
  def forget
    update_attribute(:remember_digest, nil)
  end

  private

    # アップロードされた画像のサイズをバリデーションする
    def icon_size
      if icon.size > 1.megabytes
        errors.add(:icon, "should be less than 1MB")
      end
    end

end
