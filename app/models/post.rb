class Post < ApplicationRecord
  belongs_to :user
  has_many :likes

  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 1000 }
  validate  :picture_size

  has_ancestry

  private

    def picture_size
      if picture.size > 1.megabytes
        errors.add(:picture, "should be less than 1MB")
      end
    end

end
