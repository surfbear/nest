class StaticPagesController < ApplicationController

  def home
    if logged_in?
      @post = current_user.posts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  def stream
    @post = Post.all.paginate(page: params[:page])
  end

  def search
    @q = current_user.feed.ransack(params[:q])
  end

  def result
    if  params[:q][:content_cont].blank?
      @feed_items = []
    else
      @q = current_user.feed.ransack(params[:q])
      @feed_items = @q.result.paginate(page: params[:page])
    end
  end

end
