class SessionsController < ApplicationController

  def create
    user = User.find_by(user_name: params[:session][:user_name])
    if user && user.authenticate(params[:session][:password])
      log_in user
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      redirect_back_or root_path
    else
      flash.now[:danger] = 'ユーザIDまたはパスワードが正しくありません。'
      render 'static_pages/home'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

end
