class PostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy

  def show
    @post = current_user.posts.build
    @root = Post.find_by(id: params[:id])
  end

  def create
    root = Post.find_by(id: params[:post_id])
    if !root.nil?
      send_params = post_params
      send_params.store(:user_id, current_user.id)
      @post = root.children.build(send_params)
    else
      @post = current_user.posts.build(post_params)
    end

    if @post.save
      redirect_to request.referrer || root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end

  end

  def destroy
    @post.destroy
    flash[:success] = "削除されました！"
    redirect_to request.referrer || root_url
  end

  private

    def post_params
      params.require(:post).permit(:content, :picture)
    end

    def correct_user
      @post = current_user.posts.find_by(id: params[:id])
      redirect_to root_url if @post.nil?
    end
end
