class LikesController < ApplicationController

  def index
    @likes = current_user.likes.joins(:user).joins(:post)
  end

  def create
    Like.create(user_id: current_user.id, post_id: params[:post_id])
    redirect_to request.referrer || root_url
  end

  def destroy
    Like.find_by(user_id: current_user.id, post_id: params[:id]).destroy
    redirect_to request.referrer || root_url
  end

end
