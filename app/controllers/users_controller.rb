class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy, :following, :followers]
  before_action :correct_user,   only: [:edit, :update]
  before_action :set_user,       only: [:show, :edit, :update]
  before_action :admin_user,     only: :destroy

  def index
    @users = User.where(del_flg: false).order(created_at: :desc).paginate(page: params[:page])
  end

  # プロフィールページ
  def show
    @posts = @user.posts.paginate(page: params[:page])
  end

  def new
    @user = User.new
  end

  # アカウント作成
  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      redirect_to @user, flash: {success: 'ユーザが登録されました！'}
    else
      render 'new'
    end
  end

  # アカウント編集ページ
  def edit
  end

  # アカウント編集
  def update
    if @user.update(user_params)
      flash.now[:success] = "設定が保存されました！"
    end
    render 'edit'
  end

  def destroy
    User.find_by(user_name: params[:user_name]).destroy
    flash[:success] = "ユーザID[#{params[:user_name]}]を削除しました！"
    redirect_to users_url
  end

  def following
    @title = "フォロー"
    @user = User.find_by(user_name: params[:user_name])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "フォロワー"
    @user = User.find_by(user_name: params[:user_name])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private

    def set_user
      @user  = User.find_by(user_name: params[:user_name])
    end

    # パラメータチェック
    def user_params
      params.require(:user).permit(:user_name, :user_screen_name, :profile, :password, :password_confirmation, :icon)
    end

    # 正しいユーザーかどうか確認
    def correct_user
      set_user
      redirect_to root_url unless current_user?(@user)
    end

    # 管理者かどうか確認
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

end