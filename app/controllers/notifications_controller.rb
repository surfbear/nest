class NotificationsController < ApplicationController

  def index
    @post = Post.where(user_id: current_user.id)
    @children = []
    @post.each do |post|
      post.children.each do |child|
        @children.push(child)
      end
    end
    puts "-----------------------------------------"
    @replay = Post.where("content LIKE ?", "%@#{current_user.user_name}%")
    @notifications = (@children | @replay).sort.reverse
  end

end
