module UsersHelper

  # ユーザ名を返す
  def user_name(user = @user)
    if user.user_screen_name.blank?
      user.user_name
    else
      user.user_screen_name
    end
  end

  # ユーザアイコンを返す　アイコン設定が無ければデフォルト画像
  def user_icon(user = @user)
    if user.icon?
      image_tag user.icon.url, size: '40x40'
    else
      image_tag 'no_image.png', size: '40x40'
    end
  end

  def delete_user?(user)
    current_user.admin? && !current_user?(user)
  end

end
