# Create dummy user
User.create!(user_name:             "admin",
             user_screen_name:      "管理人さん",
             profile:               "カピバラさんはいいぞ",
             password:              "111111",
             password_confirmation: "111111",
             admin: true)

10.times do |n|
  user_name = ["kapibara", "white-san", "namakemono", "ryama", "beige", "hidamari", "prairie", "atsui" ,"nomi", "Regent"]
  user_screen_name = ["カピバラさん", "ホワイトさん", "なまけものくん", "リャマ", "ベージュさん", "ひだまりさん", "プレーリーさん", "あついさん" ,"ノミくん", "リーゼントくん"]
  password = "111111"
  User.create!(user_name:             user_name[n],
               user_screen_name:      user_screen_name[n],
               profile:               Faker::Lorem.sentence(5),
               password:              password,
               password_confirmation: password
  )
end

# Create dummy posts
users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.posts.create!(content: content) }
end

# RelationShips
users = User.all
user  = users.first
following = users[2..10]
followers = users[3..5]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }