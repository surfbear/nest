class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string  :user_name, unique: true
      t.string  :user_screen_name
      t.string  :password_digest
      t.string  :remember_digest
      t.string  :icon
      t.text    :profile
      t.boolean :admin, default: false
      t.boolean :del_flg, default: false
      t.timestamps
    end
  end
end
