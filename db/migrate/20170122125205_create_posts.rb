class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.text :content
      t.references :user, foreign_key: true
      t.string :picture
      t.string :ancestry, :index => true
      t.timestamps
    end
    add_index :posts, [:user_id, :ancestry, :created_at]
  end
end
